import java.sql.*;

public class KoneksiDB {

    public static Connection con;
    public static Statement stm;

    public void KoneksiMySQL() {
        try {

            Class.forName("com.mysql.cj.jdbc.Driver");
            String DB_URL = "jdbc:mysql://localhost:3306/penjualan";
            String USER = "root";
            String PASS = "";

            con = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);
            stm = con.createStatement();
            System.out.println("Koneksi Berhasil");
        } catch (Exception e) {
            System.out.println("Koneksi Gagal");
        }
    }

    public Connection getCon() {
        return con;
    }

    public Statement getStm() {
        return stm;
    }
}
