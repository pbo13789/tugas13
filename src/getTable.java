import javax.swing.table.DefaultTableModel;
import java.sql.*;

public class getTable {
    public static DefaultTableModel getTableModel() {
        DefaultTableModel tableModel = new DefaultTableModel();

        try {
            KoneksiDB koneksiDB = new KoneksiDB();
            koneksiDB.KoneksiMySQL();

            // Membuat statement SQL
            Statement statement = koneksiDB.getCon().createStatement();
            String query = "SELECT * FROM barang";
            ResultSet resultSet = statement.executeQuery(query);

            // Mendapatkan metadata kolom
            ResultSetMetaData metaData = resultSet.getMetaData();
            int columnCount = metaData.getColumnCount();

            // Menambahkan kolom ke model tabel
            for (int col = 1; col <= columnCount; col++) {
                tableModel.addColumn(metaData.getColumnName(col));
            }

            // Menambahkan data ke model tabel
            while (resultSet.next()) {
                Object[] rowData = new Object[columnCount];
                for (int col = 1; col <= columnCount; col++) {
                    rowData[col - 1] = resultSet.getObject(col);
                }
                tableModel.addRow(rowData);
            }

            // Menutup statement dan koneksi
            statement.close();
            koneksiDB.getCon().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tableModel;
    }
}
